#!/usr/bin/env node

import { silenceExec } from './lib/cli.js';

const args = process.argv.slice(2);

if (args.includes('-')) {
  process.stdin.setEncoding('utf-8');

  let input = '';

  process.stdin.on('data', (chunk) => {
    input += chunk;
  });

  process.stdin.on('end', () => silenceExec(args, input));
} else {
  silenceExec(args);
}
