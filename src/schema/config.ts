/* eslint-disable max-len */

import VuedocParserSchema from './options.js';

export default {
  type: 'object',
  properties: {
    join: {
      type: 'boolean',
      default: false,
      description: 'Combine generated documentation for multiple component files into only one',
    },
    output: {
      type: 'string',
      minLength: 1,
      description: 'The output of the documentation. Can be a directory or a JSON file. If absent, the STDOUT will be used',
    },
    filenames: {
      type: 'array',
      items: {
        type: 'string',
        minLength: 1,
      },
      description: 'List of component filenames to parse and render. If `options.join === true` options.filenames will be rendered as a single component',
      default: [],
    },
    parsing: VuedocParserSchema,
  },
  additionalProperties: false,
};
