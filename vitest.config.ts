// eslint-disable-next-line import/extensions
import { defineConfig } from 'vitest/config';

export default defineConfig({
  test: {
    reporters: process.env.UPDATE_EXAMPLES_RESULTS ? 'verbose' : 'default',
    setupFiles: [
      '@vuedoc/test-utils',
      'test/setup.js',
    ],
    coverage: {
      include: [
        'src/**',
      ],
    },
    watchExclude: [
      'test/examples/**',
      'esm/**',
    ],
  },
});
