{ pkgs ? import <nixpkgs> {} }:

with pkgs; pkgs.mkShell {
  name = "vuedoc";
  nativeBuildInputs = [
    # tmux
    git
    tmux
    gitmux
    tmuxinator

    # devtools
    yarn
    nodejs-18_x
  ];
  shellHook = ''
    mkdir -p .nix-node
    export NODE_PATH=$PWD/.nix-node
    export PATH=$NODE_PATH/bin:$PATH
  '';
}
