export default {
  parsing: {
    features: ['name', 'description', 'keywords', 'slots', 'props', 'events', 'methods'],
    loaders: [],
  },
};
