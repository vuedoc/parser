// invalid config: 'namex' is an invalid value for options.parsing.features

export default {
  unknown_option: 'true',
  parsing: {
    features: ['name', 'description', 'keywords', 'slots', 'props', 'events', 'methods'],
    loaders: [],
  },
};
