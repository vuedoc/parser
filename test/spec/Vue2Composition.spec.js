import { describe, expect, it } from 'vitest';

describe('Vue 2 Composition API', () => {
  it('should handle explicit import for composition fname', async () => {
    const options = {
      filecontent: `
        <script>
          import { ref } from 'vue'

          export default {
            data() {
              return {
                /**
                 * Message value
                 */
                message: ref('Hello World!'),
              }
            },
          }
        </script>
      `,
    };

    await expect(options).toParseAs({
      errors: [],
      warnings: [],
      data: [
        {
          kind: 'data',
          name: 'message',
          type: 'string',
          description: 'Message value',
          initialValue: '"Hello World!"',
          keywords: [],
          visibility: 'public',
        },
      ],
    });
  });
});
