import fs, { readFileSync } from 'fs';

import { fileURLToPath } from 'url';
import { VuedocParser } from '../../src/parsers/VuedocParser.ts';
import { spawn } from 'child_process';
import { readFile } from 'fs/promises';
import { beforeEach, afterEach, describe, it, expect, vi } from 'vitest';
import { exec, parseArgs, processContent, processRawContent, silenceExec } from '../../src/lib/cli.ts';
import { dirname, join } from 'path';
import { cwd } from 'process';

const __dirname = dirname(fileURLToPath(import.meta.url));

const fixturesPath = join(__dirname, '../fixtures');
const genericOutputFile = join(fixturesPath, 'component.json');
const notfoundfile = join(fixturesPath, 'notfound.vue');
const checkboxfile = join(fixturesPath, 'checkbox.simple.vue');
const checkboxResultFile = join(fixturesPath, 'checkbox.simple.result.json');

const voidfile = '/tmp/void.vue';
const packageFilename = join(__dirname, '../../package.json');
const vuecomponent = join(fixturesPath, 'vuecomponent.vue');
const vuedocConfigFile = join(fixturesPath, 'vuedoc.config.js');
const invalidVuedocConfigFile = join(fixturesPath, 'vuedoc.invalid.config.js');

const fileSystem = {
  [genericOutputFile]: '{}',
  [voidfile]: vuecomponent,
};

const { name, version } = JSON.parse(fs.readFileSync(packageFilename));

const defaultOptions = {
  join: false,
  stream: true,
  filenames: [],
  parsing: {
    features: VuedocParser.SUPPORTED_FEATURES,
  },
};

describe('cli', () => {
  let processExit;
  let processStdout;
  let processStderr;

  beforeEach(() => {
    processExit = vi.spyOn(process, 'exit');
    processStdout = vi.spyOn(process.stdout, 'write');
    processStderr = vi.spyOn(process.stderr, 'write');

    vi.mock('fs', async () => {
      const originalFs = await vi.importActual('fs');
      const mock = {
        ...originalFs,
        // createWriteStream: vi.spyOn(originalFs, 'createWriteStream'),
        readFileSync: (file) => (file in fileSystem ? fileSystem[file] : originalFs.readFileSync(file, 'utf8')),
      };

      const x = { ...mock, default: mock };

      vi.spyOn(x, 'createWriteStream');

      return x;
    });

    vi.mock('fs/promises', async () => {
      const internalFs = {};
      const originalFs = await vi.importActual('fs/promises');
      const mock = {
        ...originalFs,
        async readFile(file, encoding = 'utf8') {
          if (file in internalFs) {
            if (encoding) {
              return internalFs[file];
            }

            return {
              toString: () => internalFs[file],
            };
          }

          if (file in fileSystem) {
            if (encoding) {
              return fileSystem[file];
            }

            return {
              toString: () => fileSystem[file],
            };
          }

          return originalFs.readFile(file, encoding);
        },
        async writeFile(file, content) {
          internalFs[file] = content;
        },
      };

      return { ...mock, default: mock };
    });
  });

  afterEach(() => {
    processExit.mockReset();
    processStdout.mockReset();
    processStderr.mockReset();
    vi.restoreAllMocks();
  });

  describe('parseArgs(argv)', () => {
    describe.each(['-v', '--version'])('%s', (arg) => {
      it(`should display package version with ${arg}`, async () => {
        const args = [arg];
        const expected = `${name} v${version}\n`;

        await silenceExec(args);
        expect(processStderr).toHaveBeenCalledTimes(0);
        expect(processStdout).toHaveBeenCalledWith(expected);
        expect(processExit).toHaveBeenCalledTimes(0);
      });
    });

    describe.each(['-c', '--config'])('%s', (arg) => {
      it('should successfully parse with missing config value', () => {
        const argv = [arg];

        expect(() => parseArgs(argv)).not.toThrow();
      });

      it('should failed with invalid config value', () => {
        const argv = [arg, 'no_found_file.js'];

        expect(() => parseArgs(argv)).rejects.toThrowError(/Failed to load/);
      });

      it('should successfully set the parsing config option', () => {
        const argv = [arg, vuedocConfigFile];

        expect(parseArgs(argv)).resolves.toEqual({
          ...defaultOptions,
          parsing: {
            ...defaultOptions.parsing,
            features: ['name', 'description', 'keywords', 'slots', 'props', 'events', 'methods'],
            loaders: [],
          },
        });
      });
    });

    describe.each(['-o', '--output'])('%s', (arg) => {
      it('should failed with missing level value', () => {
        const argv = [arg];

        expect(() => parseArgs(argv)).rejects.toThrowError(/Missing output value/);
      });

      it('should successfully set the output option', () => {
        const output = fixturesPath;
        const argv = [arg, output];

        expect(parseArgs(argv)).resolves.toEqual({ ...defaultOptions, output });
      });
    });

    describe.each(['-j', '--join'])('%s', (arg) => {
      it('should successfully set the join option', () => {
        const argv = [arg];

        expect(parseArgs(argv)).resolves.toEqual({ ...defaultOptions, join: true });
      });
    });

    describe.each(defaultOptions.parsing.features)('--ignore-%s', (feature) => {
      it(`should successfully set the ignore-${feature} option`, () => {
        const argv = [`--ignore-${feature}`];
        const expected = {
          ...defaultOptions,
          parsing: {
            ...defaultOptions.parsing,
            features: defaultOptions.parsing.features.filter((item) => item !== feature),
          },
        };

        expect(parseArgs(argv)).resolves.toEqual(expected);
      });
    });

    describe('filenames', () => {
      it('should successfully set files', () => {
        const filenames = ['/tmp/checkbox.vue', '/tmp/textarea.vue'];
        const argv = filenames;
        const expected = { ...defaultOptions, filenames };

        expect(parseArgs(argv)).resolves.toEqual(expected);
      });
    });
  });

  describe('parseArgs(argv, requireFiles)', () => {
    it('should failed with missing files', () => {
      expect(() => parseArgs([], true)).rejects.toThrowError(/Missing filename/);
    });

    it('should successfully set files', () => {
      const filenames = ['/tmp/checkbox.vue', '/tmp/textarea.vue'];
      const argv = filenames;
      const expected = { ...defaultOptions, filenames };

      expect(parseArgs(argv, true)).resolves.toEqual(expected);
    });
  });

  describe('processRawContent(argv, componentRawContent)', () => {
    it('should fail to generate documentation for an invalid component (javascript syntax error)', () => {
      const argv = [];
      const componentRawContent = `
        <template>
          <input @click="input"/>
        </template>
        <script>var invalid js code = !;</script>
      `;

      expect(processRawContent(argv, componentRawContent)).rejects.toThrow();
    });

    it('should successfully generate the component documentation', async () => {
      const argv = [];
      const componentRawContent = await readFile(checkboxfile);
      const checkboxResultJson = JSON.parse(readFileSync(checkboxResultFile));
      const result = await processRawContent(argv, componentRawContent);

      expect(result).toHaveLength(1);
      expect([JSON.parse(result[0])]).toEqual([checkboxResultJson]);

      expect(processExit).toHaveBeenCalledTimes(0);
      expect(processStderr).toHaveBeenCalledTimes(0);
      expect(processStdout).toHaveBeenCalledWith(result[0]);
    });
  });

  describe('processContent(options)', () => {
    const output = genericOutputFile;

    describe('should successfully generate the component documentation', () => {
      const filenames = [
        join(fixturesPath, 'void.vue'),
      ];

      it('should successfully generate the component documentation', () => {
        const options = { output, filenames };

        return processContent(options);
      });

      it('should successfully generate the component documentation with output directory', () => {
        const output = fixturesPath;
        const options = { output, filenames };

        return processContent(options);
      });

      it('with --join', async () => {
        const file1 = join(fixturesPath, 'join.component.1.js');
        const file2 = join(fixturesPath, 'join.component.2.vue');

        const filenames = [file1, file2];
        const options = { join: true, filenames };
        const expected = readFileSync(join(fixturesPath, 'join.result.json'));

        await processContent(options);
        expect(processStdout).toBeCalledWith(expected);
      });
    });

    describe('should failed to generate the component documentation', () => {
      it('without not found file', () => {
        const filenames = [notfoundfile];
        const options = { output, filenames };

        expect(processContent(options)).rejects.toThrow();
      });
    });
  });

  describe('processWithoutOutputOption(options)', () => {
    it('should failed to generate the component documentation', () => {
      const options = { filenames: [notfoundfile] };

      expect(() => processContent(options)).rejects.toThrow(/Cannot find module/);
    });

    it('should successfully generate the component documentation', () => {
      const options = { filenames: [checkboxfile] };

      return processContent(options);
    });

    it('should successfully generate the component documentation with --join', async () => {
      const file1 = join(fixturesPath, 'join.component.1.js');
      const file2 = join(fixturesPath, 'join.component.2.vue');

      const filenames = [file1, file2];
      const options = { join: true, filenames };
      const expected = readFileSync(join(fixturesPath, 'join.result.json'));

      await processContent(options);
      expect(processStdout).toHaveBeenCalledWith(expected);
    });
  });

  describe('exec(argv, componentRawContent)', () => {
    it('should successfully generate the component documentation', async () => {
      const argv = [];
      const filename = checkboxfile;
      const componentRawContent = await readFile(filename, 'utf8');

      await exec(argv, componentRawContent);
    });
  });

  describe('exec(argv)', () => {
    it.todo('should successfully print version with --version', () => new Promise((resolve) => {
      const expected = `@vuedoc/parser v${version}\n`;
      const cli = spawn('node', [join(cwd(), 'src/cli.ts'), '--version']);

      cli.stdout.on('data', (data) => {
        expect(data.toString()).toEqual(expected);
        resolve();
      });
    }));

    it('should successfully handle invalid vuedoc config file error', async () => {
      try {
        await exec(['-c', invalidVuedocConfigFile, checkboxfile]);
        await Promise.reject(new Error('Should failed with invalid vuedoc config file'));
      } catch (err) {
        expect(err.message).toEqual('Invalid options');
        expect(err.errors.length).toBe(1);
        expect(err.errors).toEqual([
          {
            keyword: 'additionalProperties',
            properties: ['unknown_option'],
            message: 'no additional properties',
          },
        ]);
      }
    });

    it('should successfully generate the component documentation with --output', () => {
      return exec([checkboxfile, '--output', fixturesPath]);
    });

    it('should successfully generate the component documentation', async () => {
      await exec([checkboxfile]);
      const expected = readFileSync(checkboxResultFile);

      expect(processStdout).toHaveBeenCalledWith(expected);
    });

    it('should successfully generate the joined components documentation', async () => {
      const file1 = join(fixturesPath, 'join.component.1.js');
      const file2 = join(fixturesPath, 'join.component.2.vue');
      const expected = readFileSync(join(fixturesPath, 'join.result.ignore.name.json'));

      await exec(['--ignore-name', '--join', file1, file2]);

      expect(processStdout).toHaveBeenCalledWith(expected);
    });

    it('should successfully generate multiple components documentation', async () => {
      const file1 = join(fixturesPath, 'join.component.1.js');
      const file2 = join(fixturesPath, 'join.component.2.vue');

      await exec([file1, file2]);

      expect(processStdout).toHaveBeenCalledTimes(2);
    });
  });

  describe('silenceExec(argv)', () => {
    it('should successfully generate the component documentation with --output', () => {
      return silenceExec([checkboxfile]);
    });

    it('should failed with an exception', async () => {
      await silenceExec([]);
      expect(processStderr).toHaveBeenCalledWith('Missing filenames. Usage: vuedoc-json [*.{js,vue} files]...\n\n');
    });

    it('should failed promise error catching', () => silenceExec([notfoundfile]));
  });

  describe('parsing warnings output', () => {
    it('should successfully print parsing warning messages on stderr', async () => {
      const argv = [];
      const componentRawContent = `
        <script>
          export default {
            methods: {
              /**
               * @private
               */
              trigger() {
                /**
                 * Foo event description
                 *
                 * @arg {Object} {name, val} - foo event param description
                 */
                this.$emit("foo-event", { name: "foo-name", val: "voo-val" })
              }
            }
          }
        </script>
      `;

      await processRawContent(argv, componentRawContent);
      expect(processStderr).toHaveBeenCalledWith("Warn: Invalid JSDoc syntax: '{Object} {name, val} - foo event param description'\n");
    });
  });
});
